#include <iostream>
#include <conio.h>
#include <time.h>
#include <string>
#include <vector>

using namespace std;

void playround(int& round, int& mmLeft, int& moneyEarned, int& bet)
{
	cout << "Money in hand: " << moneyEarned << endl;
	cout << "MM left:  " << mmLeft << endl;
	cout << "Round: " << round << " /12" << endl;

	cout << "mm to bet: ";
	cin >> bet;
}
void displayCards(vector<string>playerCards, vector<string>tonegawasCards)
{
	cout << "Kaiji's cards in hand: " << endl;
	for (int i = 0; i < playerCards.size(); i++)
	{
		cout << "[" << i + 1 << "]" << playerCards[i] << endl;
	}
	cout << "Tonegawa's cards in hand: " << endl;
	for (int i = 0; i < tonegawasCards.size(); i++)
	{
		cout << "[" << i + 1 << "]" << tonegawasCards[i] << endl;
	}
}
void clearCards(vector<string>&playerCards, vector<string>&tonegawasCards)
{
	playerCards.clear();
	tonegawasCards.clear();
}
void startOfRound(int& round, vector<string>& playerCards, vector<string>& tonegawasCards)//push back cards
{
	if ((round >= 1 && round <= 3) || (round >= 7 && round <= 9))
	{
		playerCards.push_back("Emperor");
		for (int i = 0; i < 4; i++)
		{
			playerCards.push_back("Civillian");
		}
		tonegawasCards.push_back("Slave");
		for (int i = 0; i < 4; i++)
		{
			tonegawasCards.push_back("Civillian");
		}
	}
	else if ((round >= 4 && round <= 6) || (round >= 10 && round <= 12))
	{
		playerCards.push_back("Slave");
		for (int i = 0; i < 4; i++)
		{
			playerCards.push_back("Civillian");
		}

		tonegawasCards.push_back("Emperor");
		for (int i = 0; i < 4; i++)
		{
			tonegawasCards.push_back("Civillian");
		}
	}
}
int pickCard()
{
	int chooseCard;
	cout << "Pick a Card to play: " << endl;
	cin >> chooseCard;
	return chooseCard - 1;// assigning choosecard to the index of the vector string
}
void cardMatchups(int& chooseCard, int& bet, int& mmLeft, int& moneyEarned, int& round, vector<string>& playerCards, vector<string>& tonegawasCards)
{
	bool isDraw;
	int roll = 4; // if Draw, tonegawa roll value changes (roll--)
	do
	{
		isDraw = false;
		int  tonegawaRoll = rand() % roll;
		if ((round >= 1 && round <= 3) || (round >= 7 && round <= 9))
		{
			if (chooseCard == 0)// = Emperor card
			{
				cout << "You have played your Emperor Card" << endl;
				cout << endl;
				if (tonegawaRoll == 0) // ai plays slave card(index 0 = slave)
				{
					cout << "Tonegawa chose to play his Slave card.You lost:( " << endl;
					cout << " The drill will move " << bet << "mm" << endl;
					mmLeft -= bet;
					round++;
				}
				else if (tonegawaRoll > 0) //ai plays civillian
				{
					cout << "Tonegawa chose to play his Civillian card. You Won! " << endl;
					moneyEarned += bet * 100000;
					round++;
				}
			}
			else if (chooseCard > 0) // civillian card(playerside, index 1-4)
			{
				cout << "You played your Civillian card" << endl;
				cout << endl;
				if (tonegawaRoll == 0)//index 0(slave)
				{
					cout << "Tonegawa played his Slave card; You Won!" << endl;
					moneyEarned += bet * 1000000;
					round++;
				}
				else if (tonegawaRoll > 0)//index 1-4(civiliian cards)
				{
					cout << "Tonegawa played his civilian card. DRAW!" << endl;
					_getch();
					system("cls");
					playerCards.erase(playerCards.begin() + chooseCard);
					tonegawasCards.erase(tonegawasCards.begin() + tonegawaRoll);
					displayCards(playerCards, tonegawasCards);
					chooseCard = pickCard();
					roll--;
					isDraw = true;
					cout << "Pick again: ";
				}
			}
		}
		else
		{
			if (chooseCard == 0)//index 0(slave)
			{
				cout << "You chose to play you Slave Card " << endl;

				if (tonegawaRoll == 0)//ai plays emperor card
				{
					cout << "Tonegawa chose to play his Emperor card. You Won!" << endl;
					moneyEarned += bet * 100000;
					round++;
				}
				else if (tonegawaRoll > 0)//ai plays civillian card
				{
					cout << "Tonegawa played his civillian card. You Lost:( " << endl;
					cout << "The drill will now move " << bet << " mm " << endl;
					mmLeft -= bet;
					round++;
				}
			}
			else if (chooseCard > 0)//player plays civillian card
			{
				cout << "You played your Civillian Card" << endl;

				if (tonegawaRoll == 0)//AI plays Emperor
				{
					cout << "Tonegawa chose to play his Emperor Card. You Lost:(";
					mmLeft -= bet;
					round++;
				}
				else if (tonegawaRoll > 0)//Ai plays civiliian card
				{
					cout << "Tonegawa chose to play his Civillian Card. DRAW!";
					_getch();
					system("cls");
					playerCards.erase(playerCards.begin() + chooseCard);
					tonegawasCards.erase(tonegawasCards.begin() + tonegawaRoll);
					displayCards(playerCards, tonegawasCards);
					chooseCard = pickCard();
					roll--;
					isDraw = true;
					cout << "Pick again: " << endl;
				}
			}
		}
	} while (isDraw == true);
}

int main()
{
	srand(time(NULL));
	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;
	vector<string> playerCards;
	vector<string> tonegawasCards;
	int bet;
	int chooseCard;
	cout << "Welecome to EMPEROR CARD, Kaiji" << endl << endl;
	while (round < 13) // per round
	{
		playround(round, mmLeft, moneyEarned, bet);
		startOfRound(round, playerCards, tonegawasCards);
		displayCards(playerCards, tonegawasCards);
		chooseCard = pickCard();
		cardMatchups(chooseCard, bet, mmLeft, moneyEarned, round, playerCards, tonegawasCards);
		clearCards(playerCards, tonegawasCards);
		_getch();
		system("cls");

		if (moneyEarned >= 20000000)//other conditions for game over
		{
			cout << "You reached 20Mil Yen. You won Emperor Card!" << endl;
			break;
			_getch();
			system("cls");
		}
		else if (mmLeft <= 0)
		{
			cout << "THERE'S BLOOD EVRYWHERE! You lost an ear and the game " << endl;
			break;
			_getch();
			system("cls");
		}
		
	}
	
	_getch();
	return 0;


}