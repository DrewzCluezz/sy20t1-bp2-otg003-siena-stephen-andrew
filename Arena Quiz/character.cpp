#include "character.h"

#include "character.h"




character::character(string name, string type, int hp, int pow, int vit, int dex, int agi)
{
	mname = name;
	mtype = type;
	mhp = hp;
	mpow = pow;
	mvit = vit;
	mdex = dex;
	magi = agi;
}

void character::displaystats()
{
	cout << "Name: " << mname << endl;
	cout << "Class: " << mtype << endl;
	cout << "HP: " << mhp << endl;
	cout << "Pow: " << mpow << endl;
	cout << "Vit: " << mvit << endl;
	cout << "Dex: " << mdex << endl;
	cout << "Agi: " << magi << endl;
}

string character::getName()
{
	return mname;
}

string character::getType()
{
	return mtype;
}



int character::getHP()
{
	return mhp;
}

int character::getPow()
{
	return mpow;
}

int character::getVit()
{
	return mvit;
}

int character::getDex()
{
	return mdex;
}

int character::getAgi()
{
	return magi;
}

int character::getDamage(character*DefVit)
{
	int damage = getPow() - DefVit->getVit();
	return damage;
}

int character::hpDamage(int damage)
{
	mhp -= damage;
	return mhp;
}

int character::healingUP()
{
	int healing = mhp * 0.3;
	mhp = mhp + healing;
	return mhp;
}

bool character::
isAlive()
{
	return mhp > 0;
}

