#pragma once
#include <iostream>
#include <string>

using namespace std;

class character
{
public:

	character(string name, string type, int hp, int pow, int vit, int dex, int agi);
	string type;
	void displaystats();

	string getName();
	string getType();
	int getHP();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();
	int getDamage(character*DefVit);
	int hpDamage(int damage);
	int healingUP();
	bool isAlive();
private:
	int mhp;
	int mpow;
	int mvit;
	int mdex;
	int magi;
	string mtype;
	string mname;
};


