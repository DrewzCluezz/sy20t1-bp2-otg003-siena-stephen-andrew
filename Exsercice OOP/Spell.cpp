#include "Spell.h"
#include "Wizard.h"

using namespace std;

void Spell::activate(Wizard* target, Wizard* caster)
{
	int spellDamage = rand() % 21 + 40;
	cout << mName << " Casted a jutsu that dealt " << spellDamage << " damage" << endl;
	target->mHP -= spellDamage;
	caster->mMP -= mCost;
}
