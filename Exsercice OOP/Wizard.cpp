#include "Wizard.h"

void Wizard::attack(Wizard* target)
{
	int attackDamage = rand() % 6 + 10;
	cout << mName << " attacked for " << attackDamage << " damage" << endl << endl;
	target->mHP -= attackDamage;
	
	gainMana();
}

void Wizard::gainMana()
{
	int generateMana = rand() % 11 + 10;
	cout << "gained " << generateMana << "mana" << endl << endl;
	mMP += generateMana;
}
