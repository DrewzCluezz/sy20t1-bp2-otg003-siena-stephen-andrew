#pragma once
#include <string>
#include <time.h>
#include <iostream>
#include "Spell.h"

using namespace std;


class Wizard
{
public:
//properties;
	string mName;
	int mHP = 250;
	int mMP = 0;
	int mminDamage;
	int mmaxDamage;
	Spell* spell;
//methods
	void attack(Wizard* target);
	

	void gainMana();
};

