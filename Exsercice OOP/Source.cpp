#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"
#include <conio.h>
#include "Spell.h"

using namespace std;

void initializeWizard(Wizard* wizard, string mName, int mHP, int mMP, Spell* spell)
{
	wizard->mName = mName;
	wizard->mHP = mHP;
	wizard->mMP = mMP;	
	wizard->spell = spell;
}

void displayHUD(Wizard* wizard)
{
	cout << wizard->mName << ": " << wizard->mHP << endl;
	cout << "MP: " << wizard->mMP << endl;	
	cout << endl;
}

void playRound(Wizard* wizard, Wizard* wizard2, Spell* s1, Spell * s2)
{
	displayHUD(wizard);
	displayHUD(wizard2);
	
	if (wizard->mMP >= 50)
	{
		s1->activate(wizard2, wizard);
	}
	else
	{
		wizard->attack(wizard2);
	}
	if (wizard2->mMP >= 50)
	{
		s2->activate(wizard, wizard2);
	}
	else
	{
		wizard2->attack(wizard);
		//condition if no mana do attack else cast spell 
	}
}

void initializeSpell(Spell* spell, string mName)
{
	spell->mName = mName;
}
int main()
{
	srand(time(NULL));
	Spell* s1 = new Spell();
	initializeSpell(s1, "Tsukoyomi");
	
	Wizard* wizard = new Wizard();
	initializeWizard(wizard, "Itachi", 250, 0, s1);
	
	Spell*s2 = new Spell();
	initializeSpell(s2, "Chidori");
	
	Wizard* wizard2	= new Wizard();
	initializeWizard(wizard2, "Sasuke", 250, 0, s2);
	
	while (wizard->mHP >= 0 && wizard2->mHP >= 0)
	{
		playRound(wizard, wizard2, s1, s2);
		
		_getch();
		system("cls");
	}

	cout << "game over" << endl;

	_getch();
	return 0;

}