#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

void betAmmount(int& bet, int& playerGold)
{
	cout << "Place Bet: ";
	cin >> bet;
	  playerGold -= bet;
	cout << "Remaining Gold = " << playerGold;
}
/*
int returnAmmount(int bet, int& playerGold)//1-1
{
	cout << "Place Bet: ";
	cin >> bet;
	playerGold -= bet;
	return bet;
}
*/

void rollDice(int& Dice1, int& Dice2)
{
	Dice1 = rand() % 6 + 1;
	Dice2 = rand() % 6 + 1;
}

int addDice(int& Roll1, int& Roll2)
{
	return Roll1 + Roll2;

}
void winConditions(int& playerGold, int& bet,int playerSum, int AiSum, int playerDice1,int playerDice2)
{
	if (playerSum > AiSum)
	{
		cout << "Player Victorious" << endl;
		playerGold += bet;
		cout << "Remaining Gold: " << playerGold << endl;
	}
	else if (playerSum == AiSum)
	{
		cout << "Draw!" << endl;
		playerGold += bet;
		cout << "Remaining Gold: " << playerGold << endl;
	}
	else if (playerDice1 && playerDice2 == 1)
	{
		cout << "Snake Eyes" << endl;
		bet *= 3;
		cout << "Bet Value: " << bet << endl;
		playerGold += bet;
		cout << "Remaining Gold: " << playerGold << endl;
	}
	else if (playerSum == 2 && AiSum == 2)
	{
		cout << "Both Players drew Snake Eyes" << endl;
		bet *= 3;
		playerGold += bet;
		cout << "Remaining Gold: " << playerGold << endl;
	}
	else
	{
		cout << "Ai Victorious" << endl;
		cout << "Remaining Gold: " << playerGold << endl;
	}

}

int main()
{
	srand(time(NULL));
	
	int playerGold = 1000;
	int bet = 0;
	while (playerGold > 0 )
	{
		
		betAmmount(bet, playerGold);
		cout << endl;

		if (bet < 0 || bet > 1000)
		{
			cout << "Invalid Bet, try again" << endl;
			system("Pause");
			int playerGold = 1000;
			cout << "Remaining Gold: " << playerGold << endl;
			betAmmount(bet, playerGold);
			cout << endl;
		}
		//bet = returnammount(bet, playerGold);

	   //1-2(dice)
		int playerDice1 = 0;
		int playerDice2 = 0;
		int AiDice1 = 0;
		int AiDice2 = 0;
		int playerSum = 0;
		int AiSum = 0;

		rollDice(playerDice1, playerDice2);
		rollDice(AiDice1, AiDice2);
		cout << "Player Roll" << endl;
		cout << "Player First Roll = " << playerDice1 << endl;
		cout << "Player Second Roll = " << playerDice2 << endl;

		cout << "AI Roll" << endl;
		cout << "AI First Roll = " << AiDice1 << endl;
		cout << "AI Second Roll = " << AiDice2 << endl << endl;
		playerSum = addDice(playerDice1, playerDice2);
		cout << " Player Sum: " << playerSum << endl;
		AiSum = addDice(AiDice1, AiDice2);
		cout << " AI Sum: " << AiSum << endl;

		winConditions(playerGold, bet, playerSum, AiSum, playerDice1, playerDice2);
		
		if (playerGold <= 0)
		{
		cout << "Defeat:(";
		break;
		}
		system("pause");
		system("cls");
		cout << "Remaining Gold: " << playerGold << endl;
	}
	_getch();
	return 0;

}