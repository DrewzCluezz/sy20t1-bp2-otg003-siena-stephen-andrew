#include <iostream>
#include <conio.h>
#include <time.h>
#include <vector>
#include <string>
using namespace std;

vector<string> randomize()
{
	vector<string> inventoryFill;
	string items[4] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };
	for (int i = 0; i < 10; i++)
	{
		inventoryFill.push_back(items[rand() % 4]);
	}
	return inventoryFill;
}


int main()
{
	srand(time(NULL));
	vector<string> inventoryFill = randomize();
	string items[4] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };
	for (int i = 0; i < inventoryFill.size(); i++)
	{
		cout << inventoryFill[i] << endl;
	}
	cout << endl;
	int count = 0;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (items[i] == inventoryFill[j])
			{
				count++;
			}

		}
		cout << items[i] << " = " << count << endl;
		count = 0;
	}

	_getch();
	return 0;
}