#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <vector>
#include "Node.h"

using namespace std;
Node* memberPicksNumber(Node*head, int &memberCount)
{
	//pointer cos memberCount will change 
	int randomMember = rand() % memberCount + 1;
	Node* current = head;
	cout << "======================" << endl;
	cout << head->name << " has the cloak" << endl;
	cout << head->name << " Picks the number: " << randomMember << endl;
	cout << "======================" << endl;
	
	for (int i = 0; i < randomMember - 1; i++)
	{
		current = current->next;
	}
	//delete from list
	Node *toDelete  = current->next;
	current->next = current->next->next;
	cout << toDelete->name <<" Please step out of the line" << endl;
	current = current->next;
	delete toDelete;
	memberCount--;//decrease number of member
	
	return current;

}

Node* playRound(Node* head, int &memberCount)
{
	Node* current = head;
	//Display members
	cout << "Volunteers: " << endl;
	for (int i = 0; i < memberCount; i++)
	{
		cout << current->name << endl;
		current = current->next;
	} 
	system("pause");
	system("cls");
	//start of Round (chosenMember will be found in round 4)
	for (int round = 1; round <= 4; round++)
	{
		cout << "Round: " << round << endl;

		while (current->next != head)
		{
			cout << current->name  << endl;
			current = current->next;
		}

		cout << current->name << endl;
		current = current->next;
		head = memberPicksNumber(head, memberCount);
		current = head; //set last as 
		system("pause");
		system("cls");
	}
		return head;
}
int main()
{
	srand(time(NULL));
	int memberCount = 5;
	Node* chosenMember;
	
	Node *member1 = new Node;
	member1->name = "Alliser";

	Node *member2 = new Node;
	member2->name = "Janos";
	member1->next = member2;

	Node *member3 = new Node;
	member3->name = "Othell";
	member2->next = member3;

	Node *member4 = new Node;
	member4->name = "Sam";
	member3->next = member4;

	Node *member5 = new Node;
	member5->name = "Snow";
	member4->next = member5;
	//makes it circular
	member5->next = member1;
	
	cout << "We are in danger, we need 1 member to ask for help" << endl;
	chosenMember = playRound(member1, memberCount);

	cout << chosenMember->name << " shall be the one to send for help" << endl;
	cout << "The rest will stay and fight" << endl;
	system("pause");
	system("cls");
	return 0;

	_getch();
	return 0;
}
